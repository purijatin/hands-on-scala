package part2

import scala.util.Try


object PatternMatchingPractice {
  def main(args: Array[String]): Unit = {
    //you can try practicing here =.
    //may be even write your tests using `require`

    def head(ls: Seq[Int]) = ls match {
      case x :: Nil =>
        println("first")
        x
      case x :: xs =>
        println("second")
        x
      case Nil => "none"
    }

    println(head(Seq(1, 2, 3, 4)))
    println(head(Seq(2, 1)))
    println(head(Seq(3)))
    println(head(Seq()))

  }
}

/**
  *
  */
object PatternMatchingQuestions {

  /**
    * If the first value of pair is an Int, then it must return the Int Value.
    *
    * If the first value of pair is an String, then it must return the Int value of String
    *
    * If the first value is something else, then it must return 0*/

  def getNum(pair: (Any, Any)): Int = ???

  /**
    * Use pattern matching to obtain the size of the list.
    * You are of-course not meant to use `List.size`
    *
    * @param ls list
    * @tparam T type of list
    * @return size of list
    */
  def size[T](ls: List[T]): Int = ???


  /**
    * Implement the function which returns `n!` using pattern matching
    *
    * @param n number whose factorial has to be determined
    * @return factorial of the number
    */
  def factorial(n:Int): Int = ???


}
