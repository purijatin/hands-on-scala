package part2


object HOPractice {
  /*
  Create a list of strings: 1 to 5. i.e. List("1","2","3","4","5")
  Generate another list from above whose contents must be List(1,2,3,4,5) i.e. List[Int]. Use map for this operation
  From above list, then generate another list that is List of square of every number. i.e. is List(1,4,9,16,25)
  Filter elements from the list that is odd
  Find the element that is divisible by 3
   */

  /**
    * Converts a list of numbers to list of integers. Use `map` operation for this
    *
    * @return list of integers
    */
  def getNumbers(ls: List[String]): List[Int] = ???

  /**
    * Returns a list where an element is square of number at an index position.
    * For example `square(List(1,2,3))` would return `List(1,4,9)`
    *
    * @param ls list of numbers
    * @return square of every number
    */
  def square(ls: List[Int]): List[Int] = ???

  /**
    * Returns only odd numbers from the list
    * For example `odd(List(1,2,3))` would return `List(1,3)`
    *
    * @param ls list of numbers
    * @return odd numbers in the list
    */
  def odd(ls: List[Int]): List[Int] = ???

  /**
    * Obtain all numbers in the list that is divisible by the `divisor`.
    * For example `divisibleBy(List(1,2,3), 2)` would return `List(2)`
    *
    * @param ls
    * @param divisor
    * @return
    */
  def divisibleBy(ls: List[Int], divisor: Int): List[Int] = ???

  /**
    * If a list contains repeated elements they should be replaced with a single copy of the element.
    * The order of the elements should not be changed.
    *
    * Example:
    *
    * scala> compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    * res0: List[Symbol] = List('a, 'b, 'c, 'a, 'd, 'e)
    *
    * @param ls
    * @tparam T
    * @return
    */
  def eliminateConsecutiveDuplicates[T](ls: List[T]): List[T] = ???


}
