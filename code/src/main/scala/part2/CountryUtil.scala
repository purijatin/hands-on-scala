package part2

/**
  * Player signifies a player in a cricket team
  *
  * @param name    name of the player
  * @param country the country he resembles for
  */
case class Player(name: String, country: String)

case class Team(country: String, players: Set[Player])

object CountryUtil {

  /**
    * Returns the name of all players from the list
    * @param ls all players
    * @return
    */
  def getAllPlayerNames(ls:List[Player]):Set[String] = ???

  /**
    * Takes a list of all teams and returns the list of all players
    * @param ls list of all teams
    * @return
    */
  def getAllPlayers(ls:List[Team]):Set[Player] = ???

  /**
    * This method takes a list of all players and returns a list of teams.
    * Each team (nation) must internally contain the players it belongs.
    *
    * For example:
    *
    * ```
    * val players = List(Player("dhoni", "ind"), Player("anderson", "eng"), Player("broad", "eng"))
    *
    * getTeams(players) must return :
    * Set(
    * Team("ind", Set(Player("dhoni", "ind"))),
    * Team("eng", Set(Player("anderson", "eng"), Player("broad", "eng"))))
    * ```
    *
    * Hint: you might want to look at `groupBy` in List
    * @param ls list of all players
    * @return set of teams, where each time is grouped by their nation
    */
  def getTeams(ls: List[Player]): Set[Team] = ???
}

