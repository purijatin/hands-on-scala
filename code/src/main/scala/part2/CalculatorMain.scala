package part2


object CalculatorMain {
  def main(args: Array[String]): Unit = {
    assert(Calculator("1+1") == 2)
    assert(Calculator("1 + 1") == 2)
    assert(Calculator("1 * 1") == 1)
    assert(Calculator("1 / 1") == 1)
    assert(Calculator("1 - 1") == 0)
    assert(Calculator("1.1 - 0.1") == 1)
  }
}

/*
Fill in the blanks at `???`
 */
object Calculator {

  def add(a: Double, b: Double): Double = ???

  def sub(a: Double, b: Double): Double = ???

  def mul(a: Double, b: Double): Double = ???

  def div(a: Double, b: Double): Double = ???

  def operator(a: Double, b: Double, f: (Double, Double) => Double): Double = ???

  /**
    * Parses the line and produces the output.
    * Some argument examples: `1 + 1`, `1.3 * 23` etc.
    * The argument must be of the form: `number operator number`
    * @param line
    * @return
    * @throws IllegalArgumentException if argument is not in expected format
    */
  def apply(line: String): Double = {
    val ls = line.trim.split("[\\+\\-\\/\\*]")
    require(ls.size == 2, "mismatch in patter: " + ls.mkString(","))

    val a = ls(0).toDouble
    val b = ls(1).toDouble

    val ope: ((Double, Double) => Double) = if (line.contains("+"))
      ???
    else if (line.contains("-"))
      ???
    else if (line.contains("*"))
      ???
    else if (line.contains("/"))
      ???
    else throw new IllegalArgumentException("Unknown operator")

    operator(a, b, ope)
  }
}
