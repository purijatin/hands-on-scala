package part2
package day2

import scala.io.Source

/**
  * You have an application in production. The application runs a lot of tasks. For each tasks, it
  * logs the time taken to execute each task.
  *
  * The aim of this class, is to read the log file and calculate the mean and standard deviation of time taken to execute.
  *
  * The file contents would be of format:
  *```
  * 1sec
  * 1.23sec
  * 12.3sec
  * 432.sec
  * 12.0sec
  * ```
  * A sample file can be found at location: `resources/runtime.txt`
  */

object MeanStdDev{
  def main(args: Array[String]): Unit = {
    //You can use this class for testing purpose
    //the mean for that file would be: 1. and variance as 0
    val source = Source.fromInputStream(Thread.currentThread().getContextClassLoader.getResourceAsStream("runtime.txt"))
    val ans = new StatisticsCalculator(source)
    println(s"mean: ${ans.getMean}")
    println(s"stddev: ${ans.stdDev}")
    println(s"variance: ${ans.variance}")
  }
}

/**
  * Complete the class.
  *
  * Test case can be found at: MeanStdDevSpec
  * @param source
  */
class StatisticsCalculator(source: Source){

  /**
    * Iterates through the list and obtains the values as a double
    *
    * Hint look at: `Source#getLines`
    */
  val all: List[Double] = ???

  /**
    * For all the file contents, gets the mean of time taken to execute
    * @return
    */
  val getMean:Double = ???

  /**
    * For all the file contents, gets the popular-stddev of execution time
    *
    * Formula: http://www-cdn.sciencebuddies.org/Files/474/9/DefVarEqn.jpg
    *
    * (Summation of ((x - mean)^2)) / size
    *
    * @return
    */
  val variance:Double = ???

  /**
    * Formulat: sqrt of variance
    */
  val stdDev: Double = ???
}