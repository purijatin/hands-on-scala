class Year(val num:Int){
  var hashValue = num

  override def hashCode(): Int = hashValue

  override def toString = "Year("+num+")"

  def canEqual(other: Any): Boolean = other.isInstanceOf[Year]

  override def equals(other: Any): Boolean =
    other.isInstanceOf[Year] && num == other.asInstanceOf[Year].num
}

val y1 = new Year(2000)
val y2 = new Year(2000)
val x = scala.collection.mutable.Set(y1, y2)
x contains y1
x contains y2

y1.hashValue = 23
x contains y1
x contains y2

