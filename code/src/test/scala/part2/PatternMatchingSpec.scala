package part2

import org.scalatest.{Matchers, WordSpec}


class PatternMatchingSpec extends WordSpec with Matchers  {

  "Pattern Matching" should {
    "match appropriately" in {
      an[IllegalArgumentException] should be thrownBy PatternMatchingQuestions.factorial(-1)
      an[IllegalArgumentException] should be thrownBy PatternMatchingQuestions.factorial(-1000)
      PatternMatchingQuestions.factorial(0) shouldEqual 1
      PatternMatchingQuestions.factorial(2) shouldEqual 2
      PatternMatchingQuestions.factorial(10) shouldEqual 3628800

      PatternMatchingQuestions.size(List()) shouldEqual 0
      PatternMatchingQuestions.size(List(1)) shouldEqual 1
      PatternMatchingQuestions.size(List.range(1, 100)) shouldEqual 100

      PatternMatchingQuestions.getNum(1,2) shouldEqual 1
      PatternMatchingQuestions.getNum("10",2) shouldEqual 10
      PatternMatchingQuestions.getNum(List(),2) shouldEqual 0
    }
  }
}
