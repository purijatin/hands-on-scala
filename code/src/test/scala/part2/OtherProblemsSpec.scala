package part2

import org.scalatest.{Matchers, WordSpec}

import scala.util.{Success, Try}

/**
  * Created by jatin on 7/4/17.
  */
class OtherProblemsSpec extends WordSpec with Matchers  {

  "Try" should {
    "behave as expected" in {
      OtherProblems.getSqrt("1") shouldEqual Success(1)
      OtherProblems.getSqrt("0") shouldEqual Success(0)
      OtherProblems.getSqrt("-1") shouldEqual Success(-1)
      OtherProblems.getSqrt("1,2,3,4") shouldEqual Success(10)
      OtherProblems.getSqrt(" -1, 2  , -3,4,-5    ,    7") shouldEqual Success(4)

      OtherProblems.getSqrt("").isFailure shouldEqual true
      OtherProblems.getSqrt(" -1, 2  , s2,4,-5    ,    7").isFailure shouldEqual true
    }
  }

  "Option" should {
    "behave as expected" in {
      OtherProblems.convertToOption(Try(1)) shouldEqual Some(1)
      OtherProblems.getSqrtAsOption("1,2,3") shouldEqual Some(6)
      OtherProblems.getSqrtAsOption("cercei broke the internet") shouldEqual None
    }
  }
}
