package part2


import org.scalatest.{Matchers, WordSpec}

class CountryUtilSpec extends WordSpec with Matchers {
  "Teams" should {

    "all player names" in {
      val players = List(Player("dhoni", "ind"), Player("anderson", "eng"), Player("broad", "eng"),
        Player("kohli", "ind"), Player("steyn", "sa"))
      CountryUtil.getAllPlayerNames(players) shouldEqual Set("dhoni", "anderson", "broad", "kohli", "steyn")
      CountryUtil.getAllPlayerNames(List()) shouldEqual Set()
    }


    "players from teams" in {
      val teams =
        List(
          Team("sa", Set(Player("steyn", "sa"))),
          Team("ind", Set(Player("dhoni", "ind"), Player("kohli", "ind"))),
          Team("eng", Set(Player("anderson", "eng"), Player("broad", "eng"))))


      CountryUtil.getAllPlayers(List()) shouldEqual Set()
      CountryUtil.getAllPlayers(teams) shouldEqual Set(Player("dhoni", "ind"), Player("anderson", "eng"), Player("broad", "eng"),
        Player("kohli", "ind"), Player("steyn", "sa"))
    }


    "be grouped" in {
      val players = List(Player("dhoni", "ind"), Player("anderson", "eng"), Player("broad", "eng"),
        Player("kohli", "ind"), Player("steyn", "sa"))
      CountryUtil.getTeams(players) shouldEqual Set(
        Team("sa", Set(Player("steyn", "sa"))),
        Team("ind", Set(Player("dhoni", "ind"), Player("kohli", "ind"))),
        Team("eng", Set(Player("anderson", "eng"), Player("broad", "eng"))))

      val players2 = List(Player("dhoni", "ind"), Player("anderson", "eng"), Player("broad", "eng"))

      CountryUtil.getTeams(players2) shouldEqual Set(
        Team("ind", Set(Player("dhoni", "ind"))),
        Team("eng", Set(Player("anderson", "eng"), Player("broad", "eng"))))

    }

  }


}


