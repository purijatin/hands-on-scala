package part2

import org.scalatest.{Matchers, WordSpec}


class HOPracticeSpec extends WordSpec with Matchers {
  "HO" should {
    "simple operations defined well" in {
      HOPractice.divisibleBy(List(3, 6, 9), 3) shouldEqual List(3, 6, 9)
      HOPractice.getNumbers(List("1","2","3")) shouldEqual List(1,2,3)
      HOPractice.getNumbers(List()) shouldEqual List()
      HOPractice.odd(List(2,4,6)) shouldEqual List()
      HOPractice.odd(List(1,2,3)) shouldEqual List(1, 3)
      HOPractice.square(List(2,3,4,0)) shouldEqual List(4,9,16,0)
      HOPractice.eliminateConsecutiveDuplicates(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) shouldEqual
        List('a, 'b, 'c, 'a, 'd, 'e)

    }
  }
}
