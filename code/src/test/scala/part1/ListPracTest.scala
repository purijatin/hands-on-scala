package part1

import org.scalatest.{Matchers, WordSpec}


class ListPracTest extends WordSpec with Matchers {
  "ListPrac" should {
    "operations defined well" in {
      val ls = List(1,2,3,4,5,6,7,8,9,10)

      ListPrac.oneToTen shouldEqual ls
      ListPrac.getFirstElement(ls) shouldEqual 1
      ListPrac.getLastElement(ls) shouldEqual 10
      ListPrac.size(ls) shouldEqual 10
      ListPrac.getFirstN(ls, 6) shouldEqual List(1,2,3,4,5,6)
      ListPrac.excludeLastAndGet(ls) shouldEqual List(1,2,3,4,5,6,7,8,9)
      ListPrac.excludeHeadAndGet(ls) shouldEqual List(2,3,4,5,6,7,8,9,10)
      ListPrac.containsNum(ls,7) shouldEqual true
      ListPrac.containsNum(ls,-1) shouldEqual false
      ListPrac.getAtIndex(ls, 3) shouldEqual 4
      ListPrac.appendToHead(ls, 0) shouldEqual List(0,1,2,3,4,5,6,7,8,9,10)
      ListPrac.appendList(List(0), List(1)) shouldEqual List(0,1)
      ListPrac.penultimate(List("1","2")) shouldEqual "1"
      ListPrac.reverse(List("1","2")) shouldEqual List("2","1")


    }
  }

}
