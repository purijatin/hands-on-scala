package part1

/*
import org.scalatest.{Matchers, WordSpec}


class FractionSpec extends WordSpec with Matchers {
  "toString" should{
    "return in expected format" in {
      new Fraction(1,2).toString shouldEqual "Fraction: 1/2"
      new Fraction(2,1).toString shouldEqual "Fraction: 2/1"
    }
  }

  "getFraction" should{
    "return decimal value" in {
      new Fraction(1,2).getFraction shouldEqual 0.5
      new Fraction(2,1).getFraction shouldEqual 2
    }
  }

  "operator" should{
    "+ should add" in {
      val f = new Fraction(1,2)
      val f2 = new Fraction(3,2)
      (f + f2).getFraction shouldEqual 2
      f.getFraction shouldEqual 0.5
      f2.getFraction shouldEqual 1.5
    }

    "- should add" in {
      val f = new Fraction(1,2)
      val f2 = new Fraction(3,2)
      (f - f2).getFraction shouldEqual -1
      f.getFraction shouldEqual 0.5
      f2.getFraction shouldEqual 1.5
    }

    "/ should add" in {
      val f = new Fraction(1,2)
      val f2 = new Fraction(3,2)
      (f / f2).getFraction shouldEqual 1/3.0
      f.getFraction shouldEqual 0.5
      f2.getFraction shouldEqual 1.5
    }

    "* should add" in {
      val f = new Fraction(1,2)
      val f2 = new Fraction(3,2)
      (f * f2).getFraction shouldEqual 3/4.0
      f.getFraction shouldEqual 0.5
      f2.getFraction shouldEqual 1.5
    }
  }
}
*/
